use std::env;
use std::process::{Command};
use users;

fn main() {
    let args: Vec<String> = env::args().collect();

	validate_input(args.clone());

	users::switch::set_current_uid(0).unwrap();

	Command::new(args[1].clone())
		.args(&args[2..])
		.spawn()
		.expect("Failed to execute process")
		.wait_with_output()
		.unwrap();
}

fn validate_input(args: Vec<String>) {
	let program_name = args[0]
		.as_str()
		.split("/")
		.collect::<Vec<&str>>()
		.pop()
		.unwrap();

	if program_name != "psuedo" {
		panic!("`psuedo` must be called directly");
	}

	if args.len() < 2 {
		panic!("Invalid number of arguments - missing program to call");
	}
}