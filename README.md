# Psuedo

A bullshit implementation of `sudo` written in rust.
This is purely for my own learning purposes and should probably not be used anywhere.

I woke up one morning and saw this https://rtpg.co/2022/02/13/your-own-sudo.html and figured "Hey, why not re-write it in rust?"

# Usage
```
cargo build
sudo chown root target/debug/psuedo
sudo chmod u+s target/debug/psuedo
cargo run <some command requiring root privelages>
```
